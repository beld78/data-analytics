---
output:
  html_document: default
  pdf_document:
    latex_engine: lualatex
---
<style type="text/css">
h2 {
border-left: 10px solid #3f75b3;
padding: 5px 5px 5px 10px;
background-color: #efefef;
}
</style>
---
title: "Data Analytics Assignment 2"
author: Jonas N�hl, Keven Quach
output: 
  html_document:
    theme: spacelab
    highlight: haddock
---

# Assignment description
In this bonus exercise, the overall aim is to check the impact of the real-time feedback on energy consumption (showering) using a difference-in-difference (DiD) model. Moreover, you should, subsequently, analyze the impact of participants’ characteristics on the effectiveness of the feedback. 

# Requirements
```{r Requirements}
library(dplyr)
library(ggplot2)
library(DescTools)
```

# Data Import
```{r Data Import}
#set the directory as working directory where the files are in
data_shower <- read.csv2("../data/shower_data.csv")
data_survey <- read.csv2("../data/survey_data.csv")
#str(data_shower)
#str(data_survey)
#summary(data_shower)
#summary(data_survey)
```

# Data Cleaning
1. Exclude first shower
2. partionate data (baseline/intervention/treatment/control) with flag
```{r Data Cleaning}
#We exclude the first data set due to behaviour influence & configuration
data_shower <- filter(data_shower, Shower > 1)

#Building Groups
#Treatment are Hh with group 3-6; control are Hh with group 1-2;
data_shower <- data_shower %>% mutate(is_treatment = group %in% c(3,4,5,6))

#Baseline (Shower <=10)
data_shower <- data_shower %>% mutate(is_baseline = Shower<=10)
```

# Task 2a
Before you start to build the model, you need to derive the energy consumption from the given parameters and preprocess the data. The energy use [kWh] per shower can be estimated using the following formula (neglecting units); we assume a cold water temperature of 12 degrees and an average water heating efficiency of 65%: 

Formula:
Energy= volume * ((temperature - 12) / 0.65) * (4.184 / 3600)

Calculate the energy use per shower accordign to this formula.

## Solution
```{r Energy Consumption}
# Calculate Energy Consuption
calculateEnergy <- function(df){
   df %>% mutate(Energy_consumption = Volume*((Avgtemperature-12)/0.65)*(4.184/3600))
}

data_shower <- calculateEnergy(data_shower)
```
## Result


# Task 2b
Remove showers with a negative energy consumption. Moreover, identify and remove outliers on the dependent variable, if they exist. Justify your choice.

## Solution
1. Remove neative Energy Consumption Values
2. Identify and remove outliers on volume
  a. we truncate all values below 10 liters as we assume them to be a fill of a bucket for other purposes (e.g. cleaning)
  ******b. we winsorize all values higher than 250 l as otherwise our data would be influenced a lot by those little occurences. 
     We do not truncate them as those values can be real showers but we do not know that they are not showers like in a.******
3. Remove all outliers depending on temperature 
  a. above 43 �C as this would cause injuries and cause proteins precipitation
  b. lower than 12 as we assume this to be the water temperature of the incoming water
```{r}
#Check if volume is normal distributed
xlim <- c(0,50)
ggplot(data_shower,aes(x=Volume)) + geom_histogram(binwidth=.5) + coord_cartesian(xlim=xlim)

quantile(data_shower$Volume, probs = c(0.05, 0.999))

# Truncate Volume
data_shower <- data_shower %>% filter(Volume >= 10)
# Winsorizing Volume
#data_shower <- winsorize(data_shower$Volume, )

# Truncate Temperature
data_shower <- data_shower %>% filter(Avgtemperature >= 12)
# Truncate Temperature
data_shower <- data_shower %>% filter(Avgtemperature <= 43)

```

## Result

# Task 2c
Estimate the energy savings using a difference-in-difference (DiD) model; do not include items from the questionnaire. Then, interpret the estimated values of the DiDmodel. Do the values reflect what one would expect from a successful randomization? 

## Solution
1. Build DiD model
2. recalculate enegry use
3. Interprete results
```{r calculate error due to exogene effects 2}
#DiD estimator is the difference between differences between treatment and control group
#during intervention and baseline phase:
#(Inervention_treat. - Intervention_control) - (Baseline_treatment - Baseline_control)


# create means per household
calculateMeanPerHh <- function(fd) {
  retValues <- fd %>% group_by(Hh_ID) %>%
    summarise(avg_energy_use = mean(Energy_consumption))
  return(retValues)
}

# define groups with means per HH
treatment_base <- filter(data_shower, is_treatment == T & is_baseline == T) %>% calculateMeanPerHh()
treatment_int <- filter(data_shower, is_treatment == T & is_baseline == F) %>% calculateMeanPerHh()
control_base <- filter(data_shower, is_treatment == F & is_baseline == T) %>% calculateMeanPerHh()
control_int <- filter(data_shower, is_treatment == F & is_baseline == F) %>% calculateMeanPerHh()


dd_part_control_energy <- mean(control_base$avg_energy_use)-mean(control_int$avg_energy_use)
# a negative value means that they used more energy in the intervention than in the baseline phase

#correct treatment_int values by dd_part_control_energy
treatment_int <- mutate(treatment_int, avg_energy_use_corrected = avg_energy_use + dd_part_control_energy, energy_saving_corrected = avg_energy_use_corrected - control_int$avg_energy_use )

#a negative value means energy savings in this case
DiD_Energy_savings <- mean(treatment_int$energy_saving_corrected)
Savings_without_did = DiD_Energy_savings - dd_part_control_energy
```
## Result
We can see that the control group used slightly more energy in the intervention phase (+ 0,173 kWh in avg).
The treatment group reduced their energy use by 0.472 kWh if we neglect the DiD. If we include exogene effects (in this case a growing energy use of 0,173 kWh), the total saving effect through the intervention is an avg energy saving of 0.644 kWh.

*******Thus, the result reflects a successfull randomization?!*******

# Task 2d
Now extend the basic model by including the following variables as interacting variables of your DiDmodel. Please add each of the following variables separately to the basic model (i. e., first step: DiD with Baseline, second step: DiD with Age, ...).

- Baseline Consumption 
- Age 
- Long hair (0=no) 
- Environmental attitude (att5: ich verhalte mich auch dann umweltbewusst, wenn es erheblich h�here Kosten und M�hen verursacht ; 5 = strong agreement)

Which variables have a statistically significant influence on the saving effects? 

## Solution
***Welche Werte nehmen wir hier zum pr�fen, nur die baseline_int oder vergleichen wir das von allen haushalten??***
# + is_treatmeant * is_baseline als dummy variablen einf�gen -> m�ssen im dataset enthalten sein

```{r}

data_regression <- treatment_base %>% mutate(energy_saving = treatment_int$energy_saving_corrected)
z <- 1/sqrt(data_regression$avg_energy_use)
model_baseline <- lm(formula = energy_saving ~ avg_energy_use * is_treatmeant + is_baseline, data = data_regression )
# equal to : energy_saving ~ avg_energy_use + dummy1 + dummy2 + dummy1:dummy2
summary(model_baseline)
confint(model_baseline)
plot(model_baseline)

# Join survey data
data_join <- select(data_survey, Hh_ID, alter, X03d_longhair, att5) %>% na.omit()
data_regression <- left_join(data_regression, data_join, by = "Hh_ID")

# model with age
model_age <- lm(formula = energy_saving ~ alter, data = data_regression )
summary(model_age)
confint(model_age)
plot(model_age)

# model with longhair
model_hair <- lm(formula = energy_saving ~ X03d_longhair, data = data_regression )
summary(model_hair)
confint(model_hair)
plot(model_hair)

# model with env
model_env <- lm(formula = energy_saving ~ att5, data = data_regression )
summary(model_env)
confint(model_env)
plot(model_env)

```

## Result
In the residual plot we cannot see any pattern, what shows us a goot model.
The QQ-Plot tells us that the data is not normally distributed thus we must try a different model.
The scale-location plot is a nearly horizontal line, thus the assumption of equal variance holds.
The residual vs. Leverage plot may be an indicator, that points 132 and maybe 123 might be outliers and should be removed.

# Task 2e
Include all the variables from task d) in one single DiDmodel. Visualize the coefficients in a bar plot that also contains the 90% confidence intervals for each coefficient.

## Solution
```{r}
# model with baseline, age, longhair, env
model_baseline_age_hair_env <- lm(formula = energy_saving ~ avg_energy_use + alter +X03d_longhair + att5, data = data_regression )
summary(model_baseline_age_hair_env)
confint(model_baseline_age_hair_env)
plot(model_baseline_age_hair_env)

#Missing: Plot

```

## Result

# Task 2f
Assume you want to maximize the saving effects of an energy conservation campaign. If you can ask two questions (e.g. using a survey) before starting the campaign, which questions would you ask to determine to whom to give a device? How high would the energy saving effect be if you give devices to 50% of the participants? 

Hint: You may create a DiDonly with the participants having favorable characteristics, to compare the energy savings easily with the complete sample of the present study/data set.

## Solution
```{r}

```

## Result

