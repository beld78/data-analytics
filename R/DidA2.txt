```{r calculate error due to exogene effects}
#DiD estimator is the difference between differences between treatment and control group
#during intervention and baseline phase:
#(Intervention_treat. - Intervention_control) - (Baseline_treatment - Baseline_control)

# create means per household function
calculateMeanPerHh <- function(fd) {
  retValues <- fd %>% group_by(Hh_ID) %>%
    summarise(avg_energy_use = mean(Energy_consumption))
  return(retValues)
}

# define groups with means per HH
treatment_base <- filter(data_shower, is_treatment == T & is_intervention == F) %>% calculateMeanPerHh()
treatment_int <- filter(data_shower, is_treatment == T & is_intervention == T) %>% calculateMeanPerHh()
control_base <- filter(data_shower, is_treatment == F & is_intervention == F) %>% calculateMeanPerHh()
control_int <- filter(data_shower, is_treatment == F & is_intervention == T) %>% calculateMeanPerHh()

dd_part_control_energy <- mean(control_base$avg_energy_use)-mean(control_int$avg_energy_use)
# a negative value means that they used more energy in the intervention phase than in the baseline phase
#correct treatment_int values by dd_part_control_energy
treatment_int <- mutate(treatment_int, avg_energy_use_corrected = avg_energy_use + dd_part_control_energy, energy_saving_corrected = avg_energy_use_corrected - control_int$avg_energy_use )
#a negative value means energy savings in this case
DiD_Energy_savings <- mean(treatment_int$energy_saving_corrected)
Savings_without_did = DiD_Energy_savings - dd_part_control_energy
Savings_without_did
DiD_Energy_savings
```

data_regression <- treatment_base %>% mutate(energy_saving = treatment_int$energy_saving_corrected)